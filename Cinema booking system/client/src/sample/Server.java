/*******************************************************************************
 A Server class that creates instances of MovieObject's and has methods that
 provide necessary data needed by the controller classes.
 ******************************************************************************/
package sample;
import javafx.scene.image.Image;
import javax.json.JsonArray;
import javax.json.JsonObject;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map;

public class Server {
    ArrayList<MovieObject> list=new ArrayList<>();
    ArrayList<MovieImages> movieimages=new ArrayList<>();

    /**
     *  Constructor which creates and runs a thread that will
     *  automatically update the data stored on movies locally,
     *  in the background and the thread will terminate upon
     *  closure of program.
     * @throws IOException
     */
    public Server() throws IOException {
        startUpdating();
    }

    /**
     *  Creates and runs a thread in the background to periodacilly
     *  update the 2 arraylist on movies.
     */
    public void startUpdating(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 11;
                while (CommonMethods.running) {
                    i++;
                    boolean updateImages = false;
                    if (i >= 12) {
                        updateImages = true;
                        i = 0;
                    }
                    list.clear();
                    updateMovies(updateImages);
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /**
     *  Establishes connection with the server and retrieves
     *  relevant data which is stored in the arraylists locally.
     * @param updateImage   boolean if images should be updated or not
     */
    public void updateMovies(boolean updateImage) {
        int screenPos = 0;
        String[] name = {"format"};
        String[] value = {"json"};
        JsonObject parser = CommonMethods.connection.getData("whatson.jsp", name, value);
        if (parser.getString("status").equals("SUCCESS")) {
            JsonArray movies = parser.getJsonArray("movies");
            for (int i = 0; i < movies.size(); i++) {
                JsonObject movie = movies.getJsonObject(i);
                JsonArray slots = movie.getJsonArray("slots");
                if (slots.size() > 0) {
                    MovieObject movieDetails = new MovieObject(movie.getInt("id"), movie.getString("name"),
                            movie.getInt("rating"), movie.getString("duration"), movie.getInt("repeats"));
                    JsonArray images = movie.getJsonArray("images");
                    for (int x = 0; x < images.size(); x++) {
                        String imageName = images.getString(x);
                        if (imageName.substring(0, imageName.length() - 4).toLowerCase().endsWith("poster")) {
                            if (!updateImage) {
                                Image poster = checkLocalImage(movie.getInt("id"));
                                if (poster != null) {
                                    movieDetails.setImage(poster);
                                    break;
                                }
                            }
                            movieDetails.setImageUrl(imageName);
                            movieimages.add(new MovieImages(movie.getInt("id"), movieDetails.getImage()));
                            break;
                        }
                    }
                    for (int x = 0; x < slots.size(); x++) {
                        JsonObject slot = slots.getJsonObject(x);
                        String slotFormat = "SCREEN";
                        slotFormat += slot.getInt("screen");
                        slotFormat += "_SLOT";
                        switch (slot.getString("start")) {
                            case "12:00:00":
                                slotFormat += 1;
                                break;
                            case "15:00:00":
                                slotFormat += 2;
                                break;
                            case "18:00:00":
                                slotFormat += 3;
                                break;
                            case "21:00:00":
                                slotFormat += 4;
                                break;
                        }
                        int seatsAvailable = slot.getInt("standard");
                        boolean[] vip = new boolean[20];
                        JsonArray vipSeats = slot.getJsonArray("vip");
                        for (int y = 0; y < vipSeats.size(); y++) {
                            vip[y] = vipSeats.getBoolean(y);
                        }
                        movieDetails.addSlot(slotFormat, new MovieData(vip, seatsAvailable, slot.getInt("id")));
                    }
                    movieDetails.setTicketsold(movie.getInt("soldtoday"));
                    list.add(movieDetails);
                }
            }
        }
    }

    /**
     *  Return an array of booleans representing the booked and
     *  available seats for the vip ticket option.
     * @param movie     movie to retrieve the data from
     * @param slot      the specific slot for which the booked seats is required
     * @return          an array of booleans with false meaning available and true booked etc.
     */
    public boolean[] getBookings(String movie, String slot){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){

                Map<String,MovieData> map=list.get(i).getSlots();
                for(Map.Entry<String,MovieData> entry: map.entrySet()){
                    if(entry.getKey().equals(slot)){
                        return entry.getValue().getSeats();
                    }
                }
            }
        }
        return null;
    }

    /**
     *  Check to see if a given movies poster/image is stored
     *  locally or not.
     * @param movieid   movie to check if currently stored locally
     * @return          true if that movies poster is stored as object else null
     */
    public Image checkLocalImage(int movieid) {
        for (MovieImages image : movieimages) {
            if (image.getId() == movieid) {
                return image.getImage();
            }
        }
        return null;
    }

    /**
     *  Return an array of all movie names
     * @return      array of all movie names to be played today
     */
    public String[] getMovieNames(){
        String[] names=new String[list.size()];
        for(int i=0; i<list.size(); i++){
            names[i]=list.get(i).getName();
        }
        return names;
    }

    /**
     *  Return the name of the movie that is currently playing on a particular
     *  screen.
     * @param screen    screen to process
     * @return          the name of the movie currently playing on the screen
     */
    public String getCurrentMovieName(String screen){
        LocalDateTime now = LocalDateTime.now();
        int currenthour=now.getHour();
        String currentslot=getCurrentTime(currenthour);
        String currentmovie=findMovie(screen+"_"+currentslot);
        return currentmovie;
    }

    /**
     *  Return the name of the next movie to be played on a particular screen
     * @param screen    screen to find the next movie for
     * @return          name of the movie to be played next
     */
    public String getNextMovie(String screen){
        LocalDateTime now = LocalDateTime.now();
        int forward=now.getHour()+3;
        String slot=getCurrentTime(forward);
        String nextmovie=findMovie(screen+"_"+slot);
        return nextmovie;
    }

    /**
     *  Return the name of a movie based entirely on the timeslot
     *  passed to it
     * @param timeslot      timeslot to look for
     * @return              name of movie that is playing in that timeslot
     */
    public String findMovie(String timeslot){
        for(int i=0; i<list.size(); i++){
            String allslots=getMovieSlot(list.get(i).getName());
            String[] temp=allslots.split(" ");
            String[] slot;
            for(int j=0; j<temp.length; j++){
                slot=temp[j].split(":");
                if(slot[0].equals(timeslot)){
                    return list.get(i).getName();
                }
            }
        }
        return "";
    }

    /**
     *  Return the available number of seats for a given movie
     *  that is currently playing on a screen.
     * @param screen        screen on which the movie is being played
     * @param moviename     movie to look for
     * @return              string representation of the available seats
     */
    public String getAvailableSeats(String screen, String moviename){
        String slot=getCurrentTimeslot(screen,false);
        String allslots=getMovieSlot(moviename);
        String[] temp=allslots.split(" ");
        String[] tmp;

        for(int j=0; j<temp.length; j++){
            tmp=temp[j].split(":");
            if(tmp[0].equals(slot)){
                return tmp[1];
            }
        }
        return "";
    }

    /**
     *  Return the current timeslot such as from SLOT1 to 12 to 3
     * @param screen        the screen currently playing
     * @param formatted     check to see which format the result is required in
     * @return              the current numerical timeslot in string format
     */
    public String getCurrentTimeslot(String screen, boolean formatted){
        LocalDateTime now = LocalDateTime.now();
        String slot=getCurrentTime(now.getHour());
        if(formatted==false){
            return screen+"_"+slot;
        }

        if(slot.equals("SLOT1")){
            return "12 To 3";
        }else if(slot.equals("SLOT2")){
            return "3 To 6";
        }else if(slot.equals("SLOT3")){
            return "6 To 9";
        }else if(slot.equals("SLOT4")){
            return "9 To 12";
        }
        return "";
    }

    /**
     *  Return the current slot category the system is in,
     *  depending on the hour passed as argument
     * @param currenthour
     * @return      current slot the system is in
     */
    public String getCurrentTime(int currenthour){
        //day                                       //night
        if((currenthour>=12 && currenthour<15) || (currenthour>=0 && currenthour<3)){
            return "SLOT1";
        }else if((currenthour>=15 && currenthour<18) || (currenthour>=3 && currenthour<6)){
            return "SLOT2";
        }else if((currenthour>=18 && currenthour<21) || (currenthour>=6 && currenthour<9)){
            return "SLOT3";
        }else if((currenthour>=21 && currenthour<24) || (currenthour>=9 && currenthour<12)){
            return "SLOT4";
        }
        return "error";
    }

    /**
     *  Returns a string representation of all the slots booked
     *  for a particular movie
     * @param movie     movie to find the slots for
     * @return          string representation of all the slots
     */
    public String getMovieSlot(String movie){
        String temp="";
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                Map<String,MovieData> map=list.get(i).getSlots();
                for(Map.Entry<String,MovieData> entry: map.entrySet()){
                    temp+= entry.getKey()+":"+entry.getValue().getTotalseats()+" ";
                }
                return temp.trim();
            }
        }
        return "Error";
    }


    public String getSlotID(String movie){
        String slot=checkoutController.getSelectedMovieSlot(checkoutController.selectedscreen, checkoutController.selectedtime);
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){

                Map<String,MovieData> map=list.get(i).getSlots();
                for(Map.Entry<String,MovieData> entry: map.entrySet()){
                    if(entry.getKey().equals(slot)){
                        return ""+entry.getValue().getSlotid();
                    }
                }
            }
        }
        return "Error";
    }
    /**
     *  Return the number of slots booked for a given movie
     * @param movie     movie to calculate the number of slots for
     * @return          number of slots for that movie
     */
    public int getNumberOfSlots(String movie){
        int counter=0;
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                Map<String,MovieData> map=list.get(i).getSlots();
                for(Map.Entry<String,MovieData> entry: map.entrySet()){
                    counter++;
                }
            }
        }
        return counter;
    }

    /**
     *  Return the default movie which is simply the first 1
     *  in the list
     * @return      name of first movie in the list
     */
    public String getDefaultMovie(){
        if(list.size()>0){
            return list.get(0).getName();
        }
        return "list is empty";
    }

    /**
     *  Return the status of a movie
     * @param movie     movie to search for
     * @return          status of the movie as string
     */
    public String getStatus(String movie){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                return ""+list.get(i).getStatus();
            }
        }
        return "Error";
    }

    /**
     *  Return the Image object for a given movie
     * @param movie     movie to search for
     * @return          the image object for that movie
     */
    public Image getImage(String movie){
        Image image=null;
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                image=list.get(i).getImage();
            }
        }
        return image;
    }

    /**
     *  Return the title of a movie
     * @param movie     movie to search for
     * @return          movie title
     */
    public String getTitle(String movie){
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                return ""+list.get(i).getName();
            }
        }
        return "Error";
    }

    /**
     *  Return the imdb rating for a movie
     * @param movie     movie to look for
     * @return          its rating as a string
     */
    public String getRating(String movie){
        String m="";
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                m=""+list.get(i).getRating();
            }
        }
        return m;
    }

    /**
     *  Return the duration for a given movie
     * @param movie     movie to look for
     * @return          Duration for the movie
     */
    public String getDuration(String movie){
        String m="";
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                m=""+list.get(i).getDuration();
            }
        }
        return m;
    }

    /**
     *  Return the number of repeats left for a movie
     * @param movie     movie to look for
     * @return          number of repeats left as string
     */
    public String getRepeats(String movie){
        String m="";
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                m=""+list.get(i).getRepeats();
            }
        }
        return m;
    }

    /**
     *  Return the ticket sold for a movie
     * @param movie     movie to look for
     * @return          ticket sold for that movie as string
     */
    public String getTicketSold(String movie){
        String m="";
        for(int i=0; i<list.size(); i++){
            if(list.get(i).getName().equalsIgnoreCase(movie)){
                m=""+list.get(i).getTicketsold();
            }
        }
        return m;
    }
}
