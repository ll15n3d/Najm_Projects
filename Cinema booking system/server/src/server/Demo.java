package server;

import java.io.InputStream;
import java.util.Scanner;

/**
 *
 */
public class Demo {

  /**
   * Sets up data for a demo
   */
  public static void setUp() {
    Main.database.data.clearTables();

    System.out.println("Adding images to database. This may take a while.");
    Scanner scan = new Scanner(Demo.class.getResourceAsStream("/images.txt"));
    String[] files = scan.nextLine().split(";");
    scan.close();
    for (String file : files) {
      InputStream image = Demo.class.getResourceAsStream("/images/" + file);
      Main.database.data.addImage(image, file);
    }
    System.out.println("Finished adding images to database.");

    Main.database.data.demo();
  }

}
