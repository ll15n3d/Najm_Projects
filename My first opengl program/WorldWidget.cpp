#include <GL/glu.h>
#include "WorldWidget.h"
#include <iostream>
#include <QTimer>
#include <stdio.h>

/*-----------------------------------------------------------------------------
    Class constructor, intialises all class variables to their default values.
    Loads up all the required textures and starts timers to render the animation.
-----------------------------------------------------------------------------*/
WorldWidget::WorldWidget(QWidget *parent): QGLWidget(parent), floorsides("./textures/grasstile.png", false),
	   floortop("./textures/grassfront.png", false), earth("./textures/earth.ppm", true),
       marc("./textures/Moi.ppm", true), crate("./textures/snowsides.png", false){

    this->angle=0;
    QTimer *timer_rotation=new QTimer(this);
    connect(timer_rotation, SIGNAL(timeout()), this, SLOT(update()));
    timer_rotation->start(50);

    timer_person=new QTimer(this);
    connect(timer_person, SIGNAL(timeout()), this, SLOT(personUpdate()));
    timer_person->start(50);

    timer_snow=new QTimer(this);
    connect(timer_snow, SIGNAL(timeout()), this, SLOT(snowUpdate()));
    timer_snow->start(500);

    timer_gems=new QTimer(this);
    connect(timer_gems, SIGNAL(timeout()), this, SLOT(gemsUpdate()));
    timer_gems->start(4000);

    this->base[0]=-4.0;
    this->base[1]=0.0;
    this->base[2]=4.0;

    //default camera position
    camera[0]=1.5;
    camera[1]=1.2;
    camera[2]=1.5;

    this->setMouseTracking(true);
    this->sphere = gluNewQuadric();

    phead      = gluNewQuadric();
    pshoulders = gluNewQuadric();
    ptorso     = gluNewQuadric();
    pupperleft1 = gluNewQuadric();
    plowerleft1 = gluNewQuadric();
    pupperleft2 = gluNewQuadric();
    plowerleft2 = gluNewQuadric();
}

/*-----------------------------------------------------------------------------
    Slot function for changing the speed at which the ghost moves. Simply
    modifies the timer intervals which calls the actual function for movement.
-----------------------------------------------------------------------------*/
void WorldWidget::changePersonSpeed(int value){
    timer_person->setInterval(100-value);
}

/*-----------------------------------------------------------------------------
    Slot function for changing the sensitivity of the mouse when moving the
    camera/world using the mouse.
-----------------------------------------------------------------------------*/
void WorldWidget::changeMouseSensitivty(int value){
    mouse_sensitivity=0.00001*value;
}

/*-----------------------------------------------------------------------------
    Slot function for controlling the amount of snow that falls. Again, Simply
    modifies the timers interval to decide to how much snow should fall.
-----------------------------------------------------------------------------*/
void WorldWidget::changeSnowAmount(int value){
    timer_snow->setInterval(1100-10*value);
}

/*-----------------------------------------------------------------------------
                            Class Destructor.
-----------------------------------------------------------------------------*/
WorldWidget::~WorldWidget(){
  gluDeleteQuadric(phead);
  gluDeleteQuadric(pshoulders);
  gluDeleteQuadric(ptorso);
  gluDeleteQuadric(pupperleft1);
  gluDeleteQuadric(plowerleft1);
  gluDeleteQuadric(pupperleft2);
  gluDeleteQuadric(plowerleft2);
}

/*-----------------------------------------------------------------------------
  Called when OpenGL context is set up. Sets the background color of the window
-----------------------------------------------------------------------------*/
void WorldWidget::initializeGL(){
    glClearColor(0.3, 0.3, 0.3, 0.0);
}

/*-----------------------------------------------------------------------------
    mouseMoveEvent handler function, called when the mouse moves. If the mouse
    has been clicked and is being dragged, this function moves the position of
    the camera like FPS and allows the user to rotate the world as they desire.
    The vertical angle however is clipped to 90 degrees. The actual implementation
    is done by getting the spherical coordinates of the mouse which are then converted
    to cartesian coordinates at which point the cameras position is updated.
-----------------------------------------------------------------------------*/
void WorldWidget::mouseMoveEvent(QMouseEvent *event){ //camera original x1.5,   y1.2,   z1.5
    if (event->buttons() & Qt::LeftButton) {
        int dx = event->x() - oldx;
        int dy = event->y() - oldy;
        horiz_angle += dx * mouse_sensitivity; ///phi x
        vert_angle += dy * mouse_sensitivity;  //theta y
        if(vert_angle > 90) vert_angle = 90;
        if(vert_angle < -90) vert_angle = -90;

        ///* Spherical coordinates to cartesians
        this->camera[0] = cos(horiz_angle) * sin(vert_angle); //x
        this->camera[1]= cos(vert_angle);                     //y
        this->camera[2]= sin(horiz_angle) * sin(vert_angle);  //z
        this->paintGL();
    }
}

/*-----------------------------------------------------------------------------
  Mouse handler function for when the mouse is pressed, stores the x and y values
  of the point at which the mouse was pressed.
-----------------------------------------------------------------------------*/
void WorldWidget::mousePressEvent(QMouseEvent *eventPress){
    oldx=eventPress->x();
    oldy=eventPress->y();
}

/*-----------------------------------------------------------------------------
  Called every time the widget needs painting. Contains all core graphic elements
  that make up the program. E.g. applys lighting to the world, renders the floor
  and animations etc.
-----------------------------------------------------------------------------*/
void WorldWidget::paintGL(){
    // clear the widget
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
    this->angle=(this->angle+1)%360;

    //Lighting
	glPushMatrix();
	glLoadIdentity();
    GLfloat light_pos[] = {0., 0., 10., 1.};
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);
	glPopMatrix();

    //Rotating Earth
    glPushMatrix();
    glTranslatef(0.,7.,0.);
    glRotatef(90,1.0f,0.0f,0.0f);
    glRotatef(this->angle,0.0f,0.0f,1.0f);

    glBindTexture(GL_TEXTURE_2D,tex_obj[2]); //bind the texture. then load it into the graphics hardware:
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, earth.Width(), earth.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, earth.imageField());
    gluQuadricTexture(this->sphere,1);
    gluSphere(this->sphere,2,20,20);
    glPopMatrix();

    //moving ghost
    glPushMatrix();
    glTranslatef(this->base[0], this->base[1],  this->base[2]);
    glRotatef(this->rotatebody,0.,1.,0.);
    this->movingPerson();
    glPopMatrix();

    //the floor
    this->displayFloor();

    //small cube in the middle of the floor
    glPushMatrix();
    glTranslatef(0., 2.0, 0.);
    this->displayMiddleCube();
    glPopMatrix();

    //display falling snow and gems animation
    this->displayGemsAndSnow();

    //resets the matrix back to its default state.
    glLoadIdentity();
   	gluLookAt(camera[0],camera[1],camera[2],    0.0,0.0,0.0,       0.0,1.0,0.0);
    glRotatef(this->slidervalue,0.,1.0,0.);
	glFlush();
}

/*-----------------------------------------------------------------------------
    Renders a textured cube with with an image of Marcs face on top.
-----------------------------------------------------------------------------*/
void WorldWidget::displayMiddleCube(){
    // normals, correctly calculated for the cube faces below
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };
    this->setMaterial(1);
    glEnable(GL_TEXTURE_2D);

    //bind the required texture. then load it into the graphics hardware:
    glBindTexture(GL_TEXTURE_2D,tex_obj[4]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, crate.Width(), crate.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, crate.imageField());

    glNormal3fv(normals[0]);//front
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f( 1.0, -1.0,  1.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( 1.0,  1.0,  1.0);
    glEnd();

    glNormal3fv(normals[3]);    //right
    glBegin(GL_POLYGON);
      glTexCoord2f(0.0, 0.0);
      glVertex3f(-1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( 1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( 1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f(-1.0,  1.0, -1.0);
    glEnd();

    glNormal3fv(normals[2]);//left
    glBegin(GL_POLYGON);
      glTexCoord2f(0.0, 0.0);
      glVertex3f(-1.0, -1.0, 1.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( 1.0, -1.0, 1.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( 1.0,  1.0, 1.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f(-1.0,  1.0, 1.0);
    glEnd();

    glNormal3fv(normals[1]);//back
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f( -1.0, -1.0,  1.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( -1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( -1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D,tex_obj[3]); //bind marcs face texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, marc.Width(), marc.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, marc.imageField());

    glNormal3fv(normals[4]);//top
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f(  1.0,  1.0,  1.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f(  1.0,  1.0, -1.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( -1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( -1.0,  1.0,  1.0);
    glEnd();

    glNormal3fv(normals[5]);//bottom
    glBegin(GL_POLYGON);
      glVertex3f(  1.0,  -1.0,  1.0);
      glVertex3f(  1.0,  -1.0, -1.0);
      glVertex3f( -1.0,  -1.0, -1.0);
      glVertex3f( -1.0,  -1.0,  1.0);
    glEnd();
}

/*-----------------------------------------------------------------------------
    Renders the falling snow and gems animation by first updating the positions
    of all particles and then calling their draw function to display each particle
    on the screen. If a particle goes outside the orthographic volume, it is Simply
    deleted from the vectors. Also if a particle hits or falls on top of the Rotating
    earth, it is also deleted. This is done by doing a inside sphere test on all partciles.
    If a particle falls on top of the floor, its animation is stopped to simulate a
    accumalting snow effect.
-----------------------------------------------------------------------------*/
void WorldWidget::displayGemsAndSnow(){
    float cx=0.0, cy=7.0, cz=0.0;
    float x,y,z;
    // A point (x,y,z) is inside the sphere with center (cx,cy,cz) and radius r if
    //  ( x-cx ) ^2 + (y-cy) ^2 + (z-cz) ^ 2 < r^2

    //update gem paricles positions
    for (int i=0; i<gems.size(); i++) {
      gems.at(i).move();
      x=particles.at(i).x;
      y=particles.at(i).y;
      z=particles.at(i).z;

      if (gems.at(i).y<-10 ||
        ( (pow((x-cx),2) + pow((y-cy),2)  + pow((z-cz),2))<pow(2.0,2)   )) {
          gems.erase(gems.begin()+i);
      }else if ( (gems.at(i).y-1.0<0.2) && gems.at(i).z<=4.5 && gems.at(i).z>=-4.5 && gems.at(i).x<=4.5 && gems.at(i).x>=-4.5) {
          gems.at(i).falling=false;
      }
    }

    //update snow particles positions
    for (int i=0; i<particles.size(); i++) {
      particles.at(i).move();
      x=particles.at(i).x;
      y=particles.at(i).y;
      z=particles.at(i).z;

      if (particles.at(i).y<-10 ||
        ( (pow((x-cx),2) + pow((y-cy),2)  + pow((z-cz),2))<pow(2.0,2)   )) {
          particles.erase(particles.begin()+i);
      }else if ( (particles.at(i).y-1.0<0.2) && particles.at(i).z<=4.5 && particles.at(i).z>=-4.5 && particles.at(i).x<=4.5 && particles.at(i).x>=-4.5) {
          particles.at(i).falling=false;
      }
    }

    //render all snow particles
    for (int i=0; i<particles.size(); i++) {
        particles.at(i).draw();
    }

    //render all gem particles
    for (int i=0; i<gems.size(); i++) {
        gems.at(i).draw();
    }
}

/*-----------------------------------------------------------------------------
    Slot function for the gem timer, Creates and adds a new Gem particle
    to the gems vector.
-----------------------------------------------------------------------------*/
void WorldWidget::gemsUpdate(){
    gems.push_back(Gem(rand()%15+(-10),   10.0,   rand()%15+(-10)));
}

/*-----------------------------------------------------------------------------
    Slot function for the snow timer, Creates and adds a new Snow particle
    to the snow vector.
-----------------------------------------------------------------------------*/
void WorldWidget::snowUpdate(){
    particles.push_back(Snow(rand()%15+(-10),   10.0,   rand()%15+(-10)));
}

/*-----------------------------------------------------------------------------
    Creates and displays the ghost object from gluCylinders and gluspheres.
-----------------------------------------------------------------------------*/
void WorldWidget::movingPerson(){
    double SHOULDER_WIDTH = 3.0;
    glDisable(GL_TEXTURE_2D);

    // creating the torso
    this->setMaterial(1);
    glPushMatrix(); // save the world
    glTranslatef(0.,3.,0.);
    glRotatef(90.,1.,0.,0.);
    gluCylinder(ptorso,0.7,0.7,2,6,6);
    glPopMatrix(); // restore the world

    // shoulders
    this->setMaterial(0);
    glPushMatrix();
    glTranslatef(0.,3.,-SHOULDER_WIDTH/2);
    gluCylinder(pshoulders,0.4,0.4,3,4,4);
    glPopMatrix();

    // placing the head
    glPushMatrix(); // save the global rotation state
    glTranslatef(0., 4, 0.);
    this->setMaterial(0);
    gluSphere(phead,0.7,20,20);
    glPopMatrix();  // restore the original matrix

    // upper arms       1   right arm
    this->setMaterial(2);
    glPushMatrix();
    glTranslatef(0.,3., SHOULDER_WIDTH/2.);
    glRotatef(90,1.,0.,0.);
    glRotatef(this->armangle_right,0.,1.,0.);
    gluCylinder(pupperleft1,0.4,0.4,1,4,4);

    // Now we stay in the frame of the upper arm!!!!
    this->setMaterial(0);
    glTranslatef(0.,0.,1); // move to the end of the arm;   lower_arm_left1
    glRotatef(45,0.,1.,0.); // this will be the new origin of the frame of the lower arm
    gluCylinder(plowerleft1,0.4,0.4,1,4,4);
    glPopMatrix();

    // upper arms       2   left arm
    this->setMaterial(2);
    glPushMatrix();
    glTranslatef(0., 3., -(SHOULDER_WIDTH/2.) );
    glRotatef(90,1.,0.,0.);
    glRotatef(this->armangle_left,0.,1.,0.);
    gluCylinder(pupperleft2,0.4,0.4,1,4,4);

    // Now we stay in the frame of the upper arm!!!!
    this->setMaterial(0);
    glTranslatef(0.,0.,1); // move to the end of the arm;
    glRotatef(45,0.,1.,0.); // this will be the new origin of the frame of the lower arm
    gluCylinder(plowerleft1,0.4,0.4,1,4,4);
    glPopMatrix();
}

/*-----------------------------------------------------------------------------
    Slot function called by one of the timers. Simply moves the body of the
    ghost forward while also changing the arm angles to simulate moving arms.
    When the ghost reaches the end of the floor, it is rotated to continue moving
    on the other sides of the floor.
-----------------------------------------------------------------------------*/
void WorldWidget::personUpdate(){
    int armrate=5;

    //moving the base
    if(this->side==1){
        this->base[0]+=(8.0/40.0);
    }else if(this->side==2){
        this->base[2]-=(8.0/40.0);
    }else if(this->side==3){
        this->base[0]-=(8.0/40.0);
    }else if(this->side==4){
        this->base[2]+=(8.0/40.0);
    }

    //reset base
    if(this->side==1 && this->base[0]>=4.0){    //side 1 end
        this->rotatebody=90;
        this->side+=1;
    }else if(this->side==2 && this->base[2]<=-4.0){ //side 2 end
        this->rotatebody=180;
        this->side+=1;
    }else if(this->side==3 && this->base[0]<=-4.0){ //side 3 end
        this->rotatebody=270;
        this->side+=1;
    }else if(this->side==4 && this->base[2]>=4.0){ //side 4 end
        this->rotatebody=0;
        this->side=1;
    }

    //right arm movement
    if(armangle_right<=100 && arm_forward[0]==true){
        armangle_right+=armrate;
    }else{
        arm_forward[0]=false;
    }
    if(armangle_right>=-10 && arm_forward[0]==false){
        armangle_right-=armrate;
    }else{
        arm_forward[0]=true;
    }

    //left arm movement
    if(armangle_left>=-10 && arm_forward[1]==true){
        armangle_left-=armrate;
    }else{
        arm_forward[1]=false;
    }
    if(armangle_left<=100 && arm_forward[1]==false){
        armangle_left+=armrate;
    }else{
        arm_forward[1]=true;
    }

    //floating ghost movement
    if(floating_ghost==true && base[1]<=2.0){
        base[1]+=(2.0/20.0);
    }else if(floating_ghost==false && base[1]>=0.0){
        base[1]-=(2.0/20.0);
    }
    //floating ghost movement reset
    if(abs(base[0])==0.0 || abs(base[2])==0.0){   //centre
        floating_ghost=false;
    }
    if( (side%2!=0 && abs(base[0])==4.0) || (side%2==0 && abs(base[2])==4.0)){  //end of sides
        floating_ghost=true;
    }
}

/*-----------------------------------------------------------------------------
Sets the materials properties for a particular material determined by the parameter.
-----------------------------------------------------------------------------*/
void WorldWidget::setMaterial(int style){   //brass=0   white=1     red=2
    materialStruct* p_front;

    if(style==0){
        p_front= &brassMaterials;
    }else if(style==1){
        p_front= &whiteShinyMaterials;
    }else if(style==2){
        p_front= &red_shiny_plastic;
    }

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
}

/*-----------------------------------------------------------------------------
                Called every time the widget is resized.
-----------------------------------------------------------------------------*/
void WorldWidget::resizeGL(int w, int h){
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

    //enable features
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);

    //set up lighting
    GLfloat light_pos[] = {0., 0., 10., 1.};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);

    //load and save all textures
    glGenTextures(5,this->tex_obj); //generate 5 texture objects
    glBindTexture(GL_TEXTURE_2D, tex_obj[0]); //bind the first texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floorsides.Width(), floorsides.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, floorsides.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, tex_obj[1]); //bind the second texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floortop.Width(), floortop.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, floortop.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, tex_obj[2]); //bind the third texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, earth.Width(), earth.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, earth.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, tex_obj[3]); //bind the fourth texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, marc.Width(), marc.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, marc.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, tex_obj[4]); //bind the fifth texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, crate.Width(), crate.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, crate.imageField());
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-11.0, 11.0, -11.0, 11.0, -11.0, 11.0);
}

/*-----------------------------------------------------------------------------
    Creates the floor in the world which is basically a wide textured cube.
-----------------------------------------------------------------------------*/
void WorldWidget::displayFloor(){
    glEnable(GL_TEXTURE_2D);
    // Here are the normals, correctly calculated for the cube faces below
    GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };
    this->setMaterial(1);

    glBindTexture(GL_TEXTURE_2D,tex_obj[0]); //bind the first texture. then load it into the graphics hardware:
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floorsides.Width(), floorsides.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, floorsides.imageField());

    glNormal3fv(normals[0]);//front
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f( 5.0, -1.0,  5.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( 5.0, -1.0, -5.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( 5.0,  1.0, -5.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( 5.0,  1.0,  5.0);
    glEnd();

    glNormal3fv(normals[3]);    //right
    glBegin(GL_POLYGON);
      glTexCoord2f(0.0, 0.0);
      glVertex3f(-5.0, -1.0, -5.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( 5.0, -1.0, -5.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( 5.0,  1.0, -5.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f(-5.0,  1.0, -5.0);
    glEnd();

    glNormal3fv(normals[2]);//left
    glBegin(GL_POLYGON);
      glTexCoord2f(0.0, 0.0);
      glVertex3f(-5.0, -1.0, 5.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( 5.0, -1.0, 5.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( 5.0,  1.0, 5.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f(-5.0,  1.0, 5.0);
    glEnd();

    glNormal3fv(normals[1]);//back
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f( -5.0, -1.0,  5.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f( -5.0, -1.0, -5.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( -5.0,  1.0, -5.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( -5.0,  1.0,  5.0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D,tex_obj[1]); //bind the first texture. then load it into the graphics hardware:
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floortop.Width(), floortop.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, floortop.imageField());

    glNormal3fv(normals[4]);//top
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
      glVertex3f(  5.0,  1.0,  5.0);
      glTexCoord2f(1.0, 0.0);
      glVertex3f(  5.0,  1.0, -5.0);
      glTexCoord2f(1.0, 1.0);
      glVertex3f( -5.0,  1.0, -5.0);
      glTexCoord2f(0.0, 1.0);
      glVertex3f( -5.0,  1.0,  5.0);
    glEnd();

    glNormal3fv(normals[5]);//bottom
    glBegin(GL_POLYGON);
      glVertex3f(  5.0,  -1.0,  5.0);
      glVertex3f(  5.0,  -1.0, -5.0);
      glVertex3f( -5.0,  -1.0, -5.0);
      glVertex3f( -5.0,  -1.0,  5.0);
    glEnd();
}

/*-----------------------------------------------------------------------------
Slot function for the rotating view slider, updates and saves the sliders value.
-----------------------------------------------------------------------------*/
void WorldWidget::sliderUpdate(int value){
    this->slidervalue=value;
}
