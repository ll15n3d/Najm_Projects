#include "get_ready.h"

/*---------------------------------------------------------
main event loop which renders all content for get_ready page
---------------------------------------------------------*/
bool quit2;
void render_getreadypage(){
    if(shutdown==true)  return;
    quit2=false;
    SDL_Event e;
    load_getready_media();
    while(quit2!=true){
        while(SDL_PollEvent(&e)!=0){
            if(e.type==SDL_QUIT){
                quit2=true;
                shutdown=true;
            }
            if(e.type==SDL_MOUSEBUTTONDOWN || e.type==SDL_KEYDOWN){
                quit2=true;
            }
        }
        SDL_RenderClear(render);
        showbackground(true);
        showground(true);
        showbird(false,true);
        showinstructions();
        SDL_RenderPresent(render);
        frame++;
    }
    frame=0;
}

/*---------------------------------------------------------
    loads relevant graphics for this page into textures
---------------------------------------------------------*/
SDL_Texture* instructions=NULL;
SDL_Texture* readysign=NULL;
void load_getready_media(){
    temp=IMG_Load("graphics/instruction.png");
    instructions=SDL_CreateTextureFromSurface(render,temp);

    temp=IMG_Load("graphics/get-ready.png");
    readysign=SDL_CreateTextureFromSurface(render,temp);
}

/*---------------------------------------------------------
renders instructions and get ready sign by increasing and
decreasing the opacity at diffrent frame intervals.
---------------------------------------------------------*/
SDL_Rect inst_screen={87,191,114,130};
SDL_Rect read_screen={44,50,200,50};
int alpha=120;
bool r=false;
void showinstructions(){
    if(frame%3==0){
        if(alpha<255 && r==false){
            alpha++;
            SDL_SetTextureAlphaMod(instructions,alpha);
        } else if(alpha>=255 || r==true){
            alpha--;
            SDL_SetTextureAlphaMod(instructions,alpha);
            r=true;
            if(alpha<=120)  r=false;
        }
    }
    SDL_RenderCopy(render, instructions,NULL,&inst_screen);
    SDL_RenderCopy(render, readysign,NULL,&read_screen);
}
